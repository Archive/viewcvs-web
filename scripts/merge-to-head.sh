#!/bin/bash

if [ ! -f viewvc-install ]; then
  echo "Run this script from a viewcvs-web checkout" >&2
  exit 1
fi
if [ $# -ne 1 ]; then
  echo "usage: merge-to-head.sh new-tag" >&2
  exit 1
fi

gnome_cvsroot=`svn info | grep '^Repository Root:'  | sed 's/Repository Root: //'`
lasttag=`cat scripts/last-merge-tag`
newtag=$1

svn merge $gnome_cvsroot/tags/$lasttag $gnome_cvsroot/tags/$newtag

echo "$newtag" > scripts/last-merge-tag

