#!/bin/bash

# Quit on non-zero exit status
set -e

if [ ! -f viewvc-install ]; then
  echo "Run this script from a viewcvs-web checkout" >&2
  exit 1
fi

gnome_cvsroot=`svn info | grep '^Repository Root:'  | sed 's/Repository Root: //'`
viewcvs_cvsroot='http://viewvc.tigris.org/svn/viewvc/trunk'

date="`date +%Y-%m-%d`"

#if ! grep -q /cvsroot/viewcvs ~/.cvspass; then
#  echo "Press enter when prompted for a password"
#  cvs -d "$viewcvs_cvsroot" login
#fi

tmpdir=`mktemp -d ./export.XXXXXX` || exit 1
cd $tmpdir

# First import VIEWCVS_DIST branch
svn checkout $gnome_cvsroot/branches/VIEWCVS_DIST viewcvs-$date

# Forcefully export upstream HEAD ...
svn export $viewcvs_cvsroot viewcvs-$date --username guest --force

cd viewcvs-$date

# Add new files/directories
svn add `svn st | grep '^?' | cut -b 8-`

# import into Gnome CVS ...

svn commit -m "Import of viewcvs on $date"
svn copy $gnome_cvsroot/branches/VIEWCVS_DIST $gnome_cvsroot/tags/viewcvs-$date -m "Tagging import of viewcvs on $date"


# clean up temporary files
cd ../..
rm -rf $tmpdir

scripts/merge-to-head.sh viewcvs-$date
